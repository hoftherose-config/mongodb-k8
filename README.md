# MongoDB in Kubernetes with NFS

Terraform configuration files and kubectl configuration for setting up a kubernetes based scalable mongodb server. Implementation uploaded to GCP's Kubernetes Engine but the commands in kubectl can also be used in local or other cloud provided kubernetes clusters
